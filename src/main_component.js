// Definició del component

var tempsComponent = {
    template:
        `
          <section id="Weather" class="container notification content is-large" v-if="ciutatActual!=null">
                 
                  <div class="pt-3">
                    <h1>Ciudad: </h1>
                    <select class="dropdown_custom text"  v-model="selectedCity">
                      <option v-for="ciutat in llistaCiutats" >{{ciutat}}</option>
                    </select>
                  </div>
                    <div class="d-flex justify-content-center">

                    <h1 >{{ ciutatActual.name }}</h1>
                  </div>
                  <div class="d-flex justify-content-center">

                  <h1 class="mb-0 font-weight-bold" id="heading"> {{ciutatActual.main.temp}}º C </h1>
                  </div>
                  <div class="d-flex justify-content-center">
                  <h4>{{ ciutatActual.weather[0].description }}</h4>                  
                  </div>
                  <h2 class="my-1"> <img src="https://i.imgur.com/B9kqOzp.png" height="17px"> <span> {{ciutatActual.wind.speed}} km/h </span> </h2>
                      <h2 class="my-1"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-droplet" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M7.21.8C7.69.295 8 0 8 0c.109.363.234.708.371 1.038.812 1.946 2.073 3.35 3.197 4.6C12.878 7.096 14 8.345 14 10a6 6 0 0 1-12 0C2 6.668 5.58 2.517 7.21.8zm.413 1.021A31.25 31.25 0 0 0 5.794 3.99c-.726.95-1.436 2.008-1.96 3.07C3.304 8.133 3 9.138 3 10a5 5 0 0 0 10 0c0-1.201-.796-2.157-2.181-3.7l-.03-.032C9.75 5.11 8.5 3.72 7.623 1.82z"/>
                        <path fill-rule="evenodd" d="M4.553 7.776c.82-1.641 1.717-2.753 2.093-3.13l.708.708c-.29.29-1.128 1.311-1.907 2.87l-.894-.448z"/>
                      </svg> <span> {{ciutatActual.main.humidity}} % </span> </h2>
                  <div class="d-flex justify-content-end" id="icon"> <img v-bind:src="icon" width="100px"> </div>
          </section>
        `,

    props:['llistaCiutats'],

    data: function () {
        return {
            selectedCity: null,
            ciutatActual: null,
            icon:null
        }
    },

    methods: {
        async getWeather(ciutat) {
            var url = 'https://api.openweathermap.org/data/2.5/weather?q=' + ciutat + '&units=metric&lang=ca&appid=644da4f2a1231c6611d2e2d8abb1fc90';

            try {
                var response = await fetch(url);
                var data = await response.json();

                this.ciutatActual = data;
                this.icon="http://openweathermap.org/img/wn/"+data["weather"]["0"]["icon"]+"@2x.png"

            }
            catch(err) {
                console.log(err);
            }
        }


    },

    mounted: function () {

        this.selectedCity = this.llistaCiutats[0];
    },

    watch: {
        selectedCity () {

            this.getWeather(this.selectedCity);
        }

    }
};


// eslint-disable-next-line no-undef,no-unused-vars
var vm = new Vue({
    el: '#eltemps',
    data: {
        continenteSel:"Europa",
        continentes:[
            'Europa',
            'Asia',
            'Africa',
            'America',
            'Oceania'],
        europa: [
            "Barcelona",
            "Lleida",
            "Zaragoza",
            "Sevilla",
            "Madrid",
            "Paris",
            "Moscow"
        ],
        asia:[
            "Lahore",
            "Beijing",
            "Kabul",
            "Tehran"
        ],
        america:[
            'New york',
            'Toronto',
            'Bogota',
            'Medellin'
        ],
        oceania:[
            'Sydney',
            'Melbourne',
            'Brisbane',
            'Newcastle'
        ],
        africa:[
            'Cairo',
            'Capetown',
            'Nairobi',
            'Pretoria'
        ]


    },
    methods:{
        continenteSeleccionado(continente){
            this.continenteSel=continente;

        },
        recogerContinente(){
            console.log(this.continenteSel)
            return this.continenteSel
        }
    },

    components:{
        'temps-component': tempsComponent
    }
})



