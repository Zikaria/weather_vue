// eslint-disable-next-line no-undef
var vm = new Vue({
    el: '#eltemps',
    data: {
        ciutatActual: null,
        icon:null,
		ciutats: [
				"Barcelona",
				"Lleida",
				"Zaragoza",
				"Sevilla",
				"Madrid",
				"Paris",
				"Melbourne",
				"Moscow",
				"Pekin",
				"Marrakech",
                "Islamabad",
                "Lahore"],
        selectedCity:null,

    },created: function () {
    this.selectedCity= this.ciutats[0];
    console.log(this.selectedCity);
    },
    watch:{
        selectedCity: function (){
            this.getWeather(this.selectedCity);
            return this.selectedCity;
        },
        imageURL: function (){
        }
    },
    methods: {
        getWeather(city) {
            var url ='https://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&lang=en&appid=9006f846a22ee36ceefb297145d587b4'
            fetch(url)
                .then(function (response) {
                    return response.json()
                })
                .then(function (item) {
                    vm.ciutatActual = item;
                    vm.icon="http://openweathermap.org/img/wn/"+item["weather"]["0"]["icon"]+"@2x.png"
                    console.log(item)
                })
                .catch(function(error) {
                    console.log(error);
                })
        }
    }
})


